#include <iostream>
#include <cmath>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
    string first_name, last_name, number;
    double a,b,c;
    double median;
    cout << "Please enter your names: ";
    cin >> first_name >> last_name;
    cout << "Please enter your number in university: ";
    cin >> number;

    cout << "Please enter your marks:";
    cin >> a >> b >> c;

    if (a<2 || b<2 || c<2 || a>6 || b>6 || c>6)
    {
        cout<<"Grades are incorrect";
        return -1;
    }
else median = (a+b+c)/3;
    cout << number << " - " << fixed << setprecision(2) << median << endl;
    return 0;
}

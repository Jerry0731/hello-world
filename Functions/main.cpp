#include <iostream>
using namespace std;

double Err;
double add (double a, double b)
{
    return a+b;
}

double substract (double a, double b)
{
    return a-b;
}

double multiply (double a, double b)
{
    return a*b;
}

double divide (double a, double b)
{
    return a/b;
}
double compute (double a, double b, char operation)
{
        switch (operation)
        {
        case '+':
            return add(a,b);
        case '-':
            return substract(a,b);
        case '*':
            return multiply(a,b);
        case '/':
            return divide(a,b);
        default :
            return Err;

        }
}


int main()

{
    double a,b;
    char operation;
    while(cin >> a >> operation >> b)

    {
        double result = compute(a,b,operation);
        if (result == Err)
            cout << "Error!" << endl;
        else cout << result << endl;

    }
return 0;
}

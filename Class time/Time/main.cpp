#include<iostream>
#include<string>
#include<iomanip>
using namespace std;

 class Time
{
private:
    int h;
    int m;
    int s;
public:
    Time()
    {
        time_t now;
        time(&now);
        tm*t = localtime(&now);
        h=t -> tm_hour;
        m=t -> tm_min;
        s=t -> tm_sec;
    }

    Time(int hour, int minute, int seconds)
    {
        h = hour%24;
        m = minute%60;
        s = seconds%60;

    }
    void print()
    {
        cout<< fixed << setprecision(2) << h << " : " << m << " : " << s <<endl;
    }

    void add_seconds (int seconds)
    {
        s += seconds;
        int minutes = s/60;
        s = s%60;
        add_minutes(minutes);
    }

    void add_minutes(int minutes)
    {
        m += minutes;
        int hours = m/60;
        m = m%60;
        h = (h+hours)%24;
    }

    void set_hour(int hour)
    {
        h = h%24;
    }

    int seconds_from(Time t)
    {
        return (h*3600 + m*60 + s) - (t.h*3600 + t.m*60 +t.s);
    }
};
int main()
{
Time now;
now.print ();

//now.add_seconds(3660);
//now.print();

Time deadline(11, 10, 0);
deadline.print();
cout << deadline.seconds_from(now) << endl;

//Time left = deadline=time_from(now);
return 0;

}
